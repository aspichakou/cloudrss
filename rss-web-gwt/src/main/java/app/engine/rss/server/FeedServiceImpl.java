package app.engine.rss.server;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.engine.rss.client.FeedService;
import app.engine.rss.dao.FeedDAO;
import app.engine.rss.dao.FeedDAOImpl;
import app.engine.rss.entity.FeedEntity;
import app.engine.rss.shared.dto.FeedDTO;

@Service
public class FeedServiceImpl extends SpringGwtServlet implements
		FeedService {

	private static final long serialVersionUID = -7745736807965134047L;
	private FeedDAOImpl feedDAO;
	private FeedParserImpl feedParser;

	/*
	 * (non-Javadoc)
	 * 
	 * @see app.engine.rss.server.FeedService#addFeed(java.lang.String)
	 */
	public Long addFeed(String url) throws IllegalArgumentException {
		URL urlObj = null;
		try {
			urlObj = new URL(url);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException(e);
		}
		FeedEntity entity = null;
		
		try {
			entity = feedParser.populateFeedEntityAtom(urlObj);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		} 
		
		entity.setLink(url);
		feedDAO.addFeed(entity);
		return entity.getId();
	}
	
	public void removeFeed(Long id)
	{
		assert(id!=null);
		feedDAO.removeFeed(id);
	}

	public FeedDAO getFeedDAO() {
		return feedDAO;
	}

	@Autowired
	public void setFeedDAO(FeedDAOImpl feedDAO) {
		this.feedDAO = feedDAO;
	}
	
	@Autowired
	public void setFeedParser(FeedParserImpl feedParser) {
		this.feedParser = feedParser;
	}

	public FeedDTO getFeed(Long id) throws IllegalArgumentException {
		assert(id!=null);
		final FeedEntity feed = feedDAO.getFeed(id);
		final FeedDTO dto = EntityToDTOMapper.getDTO(feed);
		return dto;
	}

	public FeedDTO[] getFeeds() throws IllegalArgumentException {
		final List<FeedEntity> feeds = feedDAO.getFeeds();
		final List<FeedDTO> dto = EntityToDTOMapper.getDTO(feeds);
		return dto.toArray(new FeedDTO[]{});
	}

}
