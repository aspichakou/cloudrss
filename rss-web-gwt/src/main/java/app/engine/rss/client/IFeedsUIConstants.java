package app.engine.rss.client;
/**
 * Feeds page UI constants
 * @author aliaksandr_spichakou
 *
 */
public interface IFeedsUIConstants {
	public static final String NEW_FEED_INFO = "newFeedInfoContainer";

	public static final String NEW_FEED_ACTIONS = "newFeedActionsContainer";
	
	public static final String FEED_INFO_CONTAINER = "feedInfoContainer";
	
	public static final String NEW_FEED_URL_TXTBOX_ID = "newFeedUrl";
	public static final String FEED_ERROR_CONTAINER = "errorLabelContainer";
	public static final String NEW_FEED_ERROR_ID = "newFeedErrorId";
	
}
